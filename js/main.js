$("#pocetna").click(function() {
    $('html, body').animate({
        scrollTop: $(".pocetna").offset().top
    }, 2000);
});

$("#programi").click(function() {
    $('html, body').animate({
        scrollTop: $(".programi").offset().top
    }, 2000);
});

$("#galerija").click(function() {
    $('html, body').animate({
        scrollTop: $(".galerija").offset().top
    }, 2000);
});

$("#kontakt").click(function() {
    $('html, body').animate({
        scrollTop: $(".kontakt").offset().top
    }, 2000);
});

$("#programi-2").click(function() {
    $('html, body').animate({
        scrollTop: $(".programi").offset().top
    }, 2000);
});

$("#raspored").click(function() {
    $('html, body').animate({
        scrollTop: $(".raspored").offset().top
    }, 2000);
});
$(".fa-bars").on('touch click' , function() {
  $('.navigation').css('display','inline-block');
  $('.navigation > ul').addClass('mobile');
  $('.fa-bars').css('display','none');
  $('.fa-times').css('display','inline-block');
});

$(".fa-times").on('touch click' , function() {
  $('.navigation').css('display','none');
  $('.navigation > ul').removeClass('mobile');
  $('.fa-bars').css('display','inline-block');
  $('.fa-times').css('display','none');
});
$(document).ready(function(){
    $("body").smoothWheel()
});
$(document).ready(function(){

  // hide our element on page load
  $('#parallax').css('opacity', 0);
  $('#parallax').waypoint(function() {
      $('#parallax').addClass('animated fadeInLeft');
  }, { offset: '70%' });

  $('#parallax-2').css('opacity', 0);
  $('#parallax-2').waypoint(function() {
      $('#parallax-2').addClass('animated fadeInRight');
  }, { offset: '100%' });

  $('#parallax-3').css('opacity', 0);
  $('#parallax-3').waypoint(function() {
      $('#parallax-3').addClass('animated fadeInLeft');
  }, { offset: '100%' });

  $('.bodyPageFirstLevelTitle').css('opacity', 0);
  $('.bodyPageFirstLevelTitle').waypoint(function() {
      $('.bodyPageFirstLevelTitle').addClass('animated fadeInDown');
  }, { offset: '70%' });

  $('.bodyPageSecondLevelTitle').css('opacity', 0);
  $('.bodyPageSecondLevelTitle').waypoint(function() {
      $('.bodyPageSecondLevelTitle').addClass('animated fadeInDown');
  }, { offset: '70%' });

  $('.span-text-bigger').css('opacity', 0);
  $('.span-text-bigger').waypoint(function() {
      $('.span-text-bigger').addClass('animated fadeInDown');
  }, { offset: '70%' });

  $('.span-text-bigger-2').css('opacity', 0);
  $('.span-text-bigger-2').waypoint(function() {
      $('.span-text-bigger-2').addClass('animated fadeInDown');
  }, { offset: '70%' });

  $('.first-image').css('opacity', 0);
  $('.first-image').waypoint(function() {
      $('.first-image').addClass('animated fadeInLeft');
  }, { offset: '70%' });

  $('.second-image').css('opacity', 0);
  $('.second-image').waypoint(function() {
      $('.second-image').addClass('animated fadeInRight');
  }, { offset: '70%' });

  $('.third-image').css('opacity', 0);
  $('.third-image').waypoint(function() {
      $('.third-image').addClass('animated fadeInLeft');
  }, { offset: '70%' });

  $('.fourth-image').css('opacity', 0);
  $('.fourth-image').waypoint(function() {
      $('.fourth-image').addClass('animated fadeInRight');
  }, { offset: '70%' });


});
